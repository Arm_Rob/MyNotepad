﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        string path;
        string LastSavedText;
        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastSavedText != richTextBox1.Text)
            {
                if (!IsSaving())
                    return;
            }


            path = string.Empty;
            richTextBox1.Clear();
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastSavedText != richTextBox1.Text)
            {
                if (!IsSaving())
                    return;
            }


            var od = new OpenFileDialog
            {
                Filter = "txt files (*.txt)|*.txt",
                ValidateNames = true,
                Multiselect = false
            };
            if (od.ShowDialog() == DialogResult.OK)
            {
                path = od.FileName;
                var sr = new StreamReader(od.FileName);
                LastSavedText = sr.ReadToEndAsync().Result;
                richTextBox1.Text = LastSavedText;
                sr.Close();
            }
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(path))
            {
                SaveAsToolStripMenuItem_Click(null, null);
            }
            else
            {
                var sw = new StreamWriter(path);
                sw.WriteAsync(richTextBox1.Text);
                LastSavedText = richTextBox1.Text;
                Text = GetName();
                sw.Close();
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sd = new SaveFileDialog()
            {
                Filter = "txt files (*.txt)|*.txt",
                ValidateNames = true
            };

            if (sd.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sd.FileName);
                sw.WriteAsync(richTextBox1.Text);
                LastSavedText = richTextBox1.Text;
                Text = GetName();
                sw.Close();
            }

        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastSavedText != richTextBox1.Text)
            {
                if (!IsSaving())
                    return;
            }

            Application.Exit();
        }

        private void RichTextBox1_TextChanged(object sender, EventArgs e)
        {

            if (richTextBox1.Text != LastSavedText)
                Text = GetName() + "*";
            else
                Text = GetName();
        }

        private string GetName()
        {
            if (string.IsNullOrEmpty(path))
                return "Untitled";

            string name = null;
            string adress = Path.GetFileName(path);
            for (int i = 0; i < adress.Length - 4; i++)
            {
                name += adress[i];
            }
            return name;
        }
        private bool IsSaving()
        {
            DialogResult result = MessageBox.Show(
                text: "Do you want to save file \"" + GetName() + "\"",
                buttons: MessageBoxButtons.YesNoCancel, caption: "Save?",
                icon: MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                if (string.IsNullOrEmpty(path))
                    SaveAsToolStripMenuItem_Click(null, null);
                else
                    SaveToolStripMenuItem_Click(null, null);

                return true;
            }
            else if (result == DialogResult.No)
                return true;
            else
                return false;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LastSavedText != richTextBox1.Text)
            {
                if (!IsSaving())
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
